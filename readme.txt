### Twitch Emote Stats

Script to generate excel file including usage statistics for emotes of specified twitch.tv channel.
Project page: https://gitlab.com/CPUQuaeler/Twitch_Emote_Stats


### Requirements

- Python 3.7 or newer: https://www.python.org/downloads/
- pip or easy_install: https://www.google.com/search?q=python+pip
    (should come with your python installation)
- Twitch client ID: https://dev.twitch.tv/dashboard/apps
    (user specific id that needs to be obtained from Twitch)


### Installation

git clone https://gitlab.com/CPUQuaeler/Twitch_Emote_Stats.git
cd Twitch_Emote_Stats
pip install -r requirements.txt


### Usage

python emote_stats.py -i <client_id> -c cohhcarnage
    - generates statistics based on CohhCarnage's VODs over the last 7 days
    - client_id ... user specific id that needs to be obtained from Twitch
                    (https://dev.twitch.tv/dashboard/apps)

python -h
    Get usage information (select different channel, adjust date range of VODs considered,
    add custom emote keywords, ...)


### Notes

- Script was written and tested on Windows OS
- Downloading chat logs takes a while as the twitch API only returns a few lines per request
- Downloading chat logs uses a legacy API (v5) as the new API doesn't support it yet
- Downloading channel specific emote codes also requires a legacy API
