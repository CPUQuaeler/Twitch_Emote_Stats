#!/usr/bin/python

import json
import argparse
import sys
import os
import datetime
import dateutil.relativedelta

from enum import IntEnum, auto

from lib.twitch import Twitch
from lib.videos import Videos
from lib.emotes import EmoteStats
from lib.spreadsheet import Spreadsheet
from lib.util import dateToStr, strToDate, userPromp




##---------------------------------------------------------------
## Declarations / Basic Definitions / Constants
##---------------------------------------------------------------

class Range(IntEnum):
    DAYS = auto()
    MONTHS = auto()
    ALL = auto()
    RANGE = auto()

CONFIG = {
    'database': {               # Database used to store config and other relevant data
        'file': 'config.json',
    },
    'keywords': [],             # Keywords used to identify emotes
    'range': (Range.DAYS,7),    # Date range to be considered
    'out_dir': "results",       # Output directory for results
    'db_entries':               # Config entries to be saved to database
                  ['keywords', 'channel', 'clientID', 'out_dir', 'range'],
    'log_dir': "logs",          # Directory for log files
    'max_date_bucket': 50       # Max. number of buckets used for trend graph
                                #   e.g. 50, range between Jan. and Feb. will be grouped into
                                #   (31+28)/2 = 30 buckets rather than 59
                                #   (too high number makes working with the trend graph slow)
}


##---------------------------------------------------------------
## Command Line Options
##---------------------------------------------------------------

def genParser():
    parser = argparse.ArgumentParser(description='Twitch Emote Stats')
    parser.add_argument('-s', dest='safe_cfg', default=False, action="store_true",
                        help='Save cmd line arguments to default config')
    parser.add_argument('-c', dest='channel', action='store', type=str,
                        help='Twitch channel')
    parser.add_argument('-i', dest='clientID', action='store', type=str,
                        help='Client-ID required by Twitch\'s Kraken API\n'
                             'https://blog.twitch.tv/client-id-required-for-kraken-api-calls-afbb8e95f843')
    parser.add_argument('-k', dest='keywords', action='store',
                        type=lambda s: [x for x in s.split(',')],
                        help='Comma separated list of additional keywords used to identify emotes')
    parser.add_argument('-o', dest='out_dir', action='store', type=str,
                        help='Output directory (e.g. results')
    parser.add_argument('--clear', default=False, action="store_true",
                        help='Clear config from database')

    # Mutually exclusive range options
    _range = parser.add_argument_group('Range', 'Range of VODs to be processed')
    range = _range.add_mutually_exclusive_group()
    range.add_argument('--days', dest='days', type=int,
                        help='Last n days')
    range.add_argument('--months', dest='months', type=int,
                        help='Last n months')
    range.add_argument('--range', dest='range', action='store',
                        type=lambda s: [x for x in s.split('-')],
                        help='Range between two dates, e.g. YYYY.MM.DD-YYYY.MM.DD')
    range.add_argument('--all', dest='all', default=False, action="store_true",
                        help='All available VODs')
    return parser


def checkOptions(parser):
    """ Werify command line options and return config """
    # Evaluate parser
    args = parser.parse_args()

    # Adjust config
    config = CONFIG

    # Read database from file
    db = {'config': {}}
    if os.path.exists(config['database']['file']):
        _db = readDB(config)
        db = _db if _db else db
        # Clear database if requested
        if args.clear:
            writeDB(config, db)
            print("Config cleared from database\n")
        # Read valid entries from database to config
        elif ('config' in db):
            for param in config['db_entries']:
                if param in db['config']:
                    config[param] = db['config'][param]

    # Range of dates if specified
    if args.range:
        config['range'] = (Range.RANGE, verifyDateRange(args.range))
    elif args.days:
        config['range'] = (Range.DAYS,args.days)
    elif args.months:
        config['range'] = (Range.MONTHS,args.months)
    elif args.all:
        config['range'] = (Range.ALL,True)

    # Derive date_from and date_to from range
    config['date_from'],config['date_to'] = rangeToDates(config['range'][0], config['range'][1])

    # Keywords
    if args.keywords:
        config['keywords'].extend(args.keywords)

    # Channel
    if args.channel:
        config['channel'] = args.channel
    if not (('channel' in config) and config['channel']):
        parser.print_usage()
        print("ERROR: No channel name specified")
        sys.exit(1)

    # clientID
    if args.clientID:
        config['clientID'] = args.clientID
    if not (('clientID' in config) and config['clientID']):
        parser.print_usage()
        print("ERROR: No client ID specified")
        sys.exit(1)

    # Output directory
    if args.out_dir:
        config["out_dir"] = args.out_dir
    verifyOutputDir(config["out_dir"], args.out_dir != None)

    # Update and save database
    if args.safe_cfg:
        for param in config['db_entries']:
            db['config'][param] = config[param]
        writeDB(config, db)
        print("Config safed to: {0}".format(os.path.abspath(config['database']['file'])))

    # Create log file directory
    config["log_dir"] = os.path.abspath(config["log_dir"])
    if not os.path.exists(config["log_dir"]):
        try:
            print("Creating log file directory: {0}".format(config["log_dir"]))
            os.mkdir(config["log_dir"])
        except:
            print("ERROR: Failed to create directory.")
            sys.exit(1)

    # Print config
    print("Config:\n"
          "- Channel name\t\t: {channel}\n"
          "- Client ID\t\t: {clientID}\n"
          "- VOD range\t\t: {range}\n"
          "- Additional keywords\t: {keywords}\n"
          "- Output directory\t: {out_dir}\n"
          "".format(channel=config['channel'], clientID=config['clientID'],
                    range=rangeToText(config['range']), out_dir=config['out_dir'],
                    keywords=','.join(config['keywords']) if config['keywords'] else "none",
                    ))

    # Command line used to invoke this script
    config['cmd_line'] = ' '.join(sys.argv)

    return config, db


def verifyDateRange(range):
    """ Check user provided date range """
    # Check for from and to date
    if len(range) != 2:
        print("ERROR: range requires two dates (from and to), e.g. YYYY.MM.DD-YYYY.MM.DD")
        sys.exit(1)
    try:
        date_from = strToDate(range[0])
        date_to = strToDate(range[1])
    except:
        print("ERROR: failed to convert dates, please verify format YYYY.MM.DD")
        sys.exit(1)

    # Swap dates if necessary
    return reversed(range) if (date_from > date_to) else range


def rangeToDates(type, value):
    """ Determine start and end date based on specified range """
    if type == Range.DAYS:
        assert(value > 0)
        date_to = datetime.date.today()
        date_from = date_to - datetime.timedelta(days=value)
    elif type == Range.MONTHS:
        assert(value > 0)
        date_to = datetime.date.today()
        date_from = date_to - dateutil.relativedelta.relativedelta(months=value)
    elif type == Range.ALL:
        date_to = datetime.date.today()
        date_from = datetime.date(datetime.MINYEAR, 1, 1)
    elif type == Range.RANGE:
        date_to = strToDate(value[1])
        date_from = strToDate(value[0])
    else:
        assert(0)

    # print("DEBUG: From {0} to {1}"  # DEBUG
          # "".format(date_from.strftime("%Y.%m.%d"), date_to.strftime("%Y.%m.%d")))

    assert(date_from <= date_to)
    return (date_from,date_to)


def verifyOutputDir(dir, prompt_user):
    """ Create output directory as needed """
    dir = os.path.abspath(dir)

    # Create missing directory
    if not os.path.exists(dir):
        msg = "Output directory \"{0}\" does not exists. Create it? ".format(dir)
        if prompt_user and (not userPromp(msg)):
            print("ERROR: No valid output directory specified.")
            sys.exit(1)
        # Create missing directory
        try:
            print("Creating output directory: {0}".format(dir))
            os.mkdir(dir)
        except:
            print("ERROR: Failed to create directory.")
            sys.exit(1)



##---------------------------------------------------------------
## Classes / Functions
##---------------------------------------------------------------

def readDB(config):
    """ Read database from file """
    assert(os.path.exists(config['database']['file']))
    with open(config['database']['file'], 'r') as file:
        return json.load(file)


def writeDB(config, db):
    """ Read database from file """
    with open(config['database']['file'], 'w') as file:
        json.dump(db, file, indent=4)


def rangeToText(range):
    """ Convert range of VODs to be processed to string """
    assert(len(range) > 1)
    type = range[0]
    value = range[1]
    if type == Range.DAYS:
        return "last {0} days".format(value)
    elif type == Range.MONTHS:
        return "last {0} months".format(value)
    elif type == Range.ALL:
        return "all available VODs"
    elif type == Range.RANGE:
        return '-'.join(value)
    assert(0)


def getChatLogs(config):
    """ Download chat logs for videos in range """
    print("Download chat logs for videos between {0} and {1}"
          "".format(dateToStr(config['date_from']), dateToStr(config['date_to'])))
    videos = Videos(config)

    # Fetch first page
    video_list,cursor = config['twitch'].getVideos()

    # Stop at empty video page
    while (len(video_list)):
        if not videos.extendTwitchFormat(video_list):
            break;
        # Fetch next page
        video_list,cursor = config['twitch'].getVideos(cursor)

    return videos


##---------------------------------------------------------------
## Main
##---------------------------------------------------------------

if __name__ == '__main__':
    # Evaluate command line options and load database
    parser = genParser()
    config, db = checkOptions(parser)

    #
    # DEBUG: for faster debugging use early exit in videos.py: checkChatLog()
    #

    # Download chat logs for videos in range
    config['twitch'] = Twitch(config)
    videos = getChatLogs(config)

    # Init stats and get emote codes from twitchemotes.com
    estats = EmoteStats(config)

    # Calculate stats
    for video in videos:
        estats.addVideo(video)

    # Create spreadsheet
    wb = Spreadsheet(estats, config)
    print("Writing {0}".format(wb.file))
    wb.close()
