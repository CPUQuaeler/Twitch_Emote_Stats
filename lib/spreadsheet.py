#!/usr/bin/python

import sys
import os
import datetime

from xlsxwriter import Workbook

from lib.util import dateToStr, updDict


class Spreadsheet(Workbook):
    """ Create and populate spreadsheet """
    DEF_COL_WIDTH = 8.43    # Default width of excel column
    MAX_TEXT_WIDTH = 50     # Max. text width used to adjust column width in xlsx file


    def __init__(self, emote_stats, config):
        self.file = self.genFileName(config["out_dir"], config['channel'])
        self.estats = emote_stats
        self.config = config

        print("Creating spreadsheet: {0}".format(self.file))
        Workbook.__init__(self, self.file)

        self.setFormats()
        self.addSummarySheet()
        self.addTrendSheet()


    def genFileName(self, dir, channel_name):
        """ Generate unique filename in format channel_YYYY_MM_DD_II (II ... 2 digit index) """
        date = datetime.date.today().isoformat().replace('-', '_')
        idx = 0
        while True:
            file_name = ("{0}_{1}_{2:02}.xlsx".format(channel_name, date, idx))
            file_path = os.path.join(dir, file_name)
            if os.path.exists(file_path):
                idx += 1
                continue
            return os.path.abspath(file_path)


    def widthOfText(self, text, limited = True):
        """ Approximate width of character in terms of excel cell width """
        CHAR_WIDTHS = (('\'', 0.23), ('ijlI,.:;` ', 0.62), ('frtJ!()-[]\\{}', 0.7),
               ('cszL"/', 0.84), ('bdehnopquBCEFKPRX', 1.13), ('ADGHUV', 1.27),
               ('NOQ&', 1.41), ('w%', 1.56), ('mM', 1.7), ('W@', 1.84),
               # ('0123456789agkvxySTYZ#$*+<=>?^_|~', 0.99) # Most likely group assumed as default
               )
        borders = 0.5
        width = 0
        for c in text:
            char_group = list(filter(lambda x: c in x[0], CHAR_WIDTHS))
            if char_group:
                width += char_group[0][1]
            else:
                width += 0.99
        if limited:
            width = width if (width < self.MAX_TEXT_WIDTH) else self.MAX_TEXT_WIDTH
        return (width + borders)


    def setFormats(self):
        """ Set formats (font, background color, ...) """
        # Disclaimer
        self.f_discl = self.add_format({'bg_color': 'gray', 'text_v_align' : 2, 'text_wrap': True})
        self.f_discl_bold = self.add_format({'bg_color': 'gray', 'text_v_align' : 2, 'bold': True})

        # Table headers
        f_opts = {'bold': True, 'font_color': 'white', 'border': 1, 'border_color': 'white'}
        self.f_header = {
            'centre': self.add_format(updDict(f_opts, {'bg_color': '#4A82BD', 'text_h_align': 2})),
            'left': self.add_format(updDict(f_opts, {'bg_color': '#4A82BD', 'text_h_align': 1}))
            }

        # Explanations
        self.f_expl_bold = self.add_format({'bg_color': '#FF9442', 'bold': True})
        self.f_expl = self.add_format({'bg_color': '#FFC694'})

        # Table cells
        self.f_cells = [self.add_format({'bg_color': '#BDCEE7'}),
                        self.add_format({'bg_color': '#DEE7F7'})]


    def insertDisclaimer(self, sheet):
        """ Insert disclaimer at top of a sheet """
        self.disc_title = 'Disclaimer:'
        sheet.set_row(0, 30, self.f_discl)
        sheet.write(0, 0, self.disc_title, self.f_discl_bold)
        sheet.merge_range(0, 1, 0, 25,
                            'Emotes are only counted once per chat message (less spamming)\n'
                            'This file was generated for the use by MS Excel 2007 or later; other '
                            'applications (such as OpenOffice) may not support the table formats')
        sheet.set_column(0, 0, self.widthOfText(self.disc_title))


    def getJobInfo(self):
        """ Setup headers and data used for job info table """
        info_headers = ["Date generated", "Dates processed", "Channel", "Cmd line",
                        "Additional Keywords"]
        info_data = [dateToStr(datetime.date.today()),
                     'From {0} to {1}'.format(self.config['date_from'], self.config['date_to']),
                     self.config['channel'], self.config['cmd_line'],
                     ', '.join(self.config['keywords']) if self.config['keywords'] else 'none']
        return (info_headers, info_data)


    def insertTable(self, sheet, row, col, content):
        """ Insert table of sums """
        headers = content[0]
        data = content[1:]

        sheet.add_table(row, col, row+len(data), len(headers),
                            {'columns': [{'header': h} for h in headers]})

        # Headers
        sheet.write_row(row, col, headers, self.f_header['left'])

        # Data rows
        row += 1
        max_w_emote_name = 0
        for r_idx, data_row in enumerate(data):
            sheet.write_row(row+r_idx, col, data_row, self.f_cells[r_idx%2])
            # Track width of widest emote name
            max_w_emote_name = max(max_w_emote_name, self.widthOfText(data_row[0]))

        #  Set column widths
        w_arrow = self.widthOfText('__') # Drop-down arrow used in header of pivot table
        col_widths = [self.widthOfText(h) + w_arrow for h in headers]
        col_widths[0] = max(max_w_emote_name, col_widths[0])
        for c_idx, width in enumerate(col_widths):
            sheet.set_column(col+c_idx, col+c_idx, width)
        return col_widths


    def insertJobInfoTable(self, sheet, row, col, info_headers, info_data, col_cnt):
        """ Insert job info table """
        # Title
        sheet.merge_range(row, col, row, col+col_cnt, "Job Info", self.f_header['centre'])
        # Headers
        sheet.write_column(row+1, col, info_headers, self.f_header['left'])
        # Data
        for r_idx, datum in enumerate(info_data):
            sheet.merge_range(row+r_idx+1, col+1, row+r_idx+1, col+col_cnt, datum,
                              self.f_cells[r_idx%2])


    def addSummarySheet(self):
        """ Create summary sheet (job info table and table of sums) """
        print("Creating summary sheet")

        # General positions & sizes
        first_col = 1   # First column to be used
        r_info = 3      # First row of job info table
        r_delim = 2     # Delimiter between tables

        content = self.estats.getSums()

        # Create sheet
        sheet = self.add_worksheet('summary')
        self.insertDisclaimer(sheet)
        info_headers,info_data = self.getJobInfo()
        self.insertJobInfoTable(sheet, r_info, first_col, info_headers, info_data, len(content[0])-1)
        col_widths = self.insertTable(sheet, r_info + len(info_headers) + r_delim + 1,
                                      first_col, content)

        # Set width of first table column based on job info table and emote names in table of sums
        w_info_headers = max([self.widthOfText(h) for h in info_headers])
        w_first_col = max(w_info_headers, col_widths[0])
        sheet.set_column(first_col, first_col, w_first_col)


    def insertExplaination(self, sheet, row, col, c_cnt):
        """ Explanation on how trends are computed """
        sheet.write(row, col, "Note:", self.f_expl_bold)
        sheet.merge_range(row, col+1, row, col+c_cnt+1,
                          "Usage counts are presented relative to the number of hours streamed "
                          "in the corresponding time frame", self.f_expl)


    def addTrendSheet(self):
        """ Create trend sheet (trend table and chart) """
        print("Creating trend sheet")

        # General positions & sizes
        r_expl = 3     # Explaination
        r_table = 5     # First row of trend table
        c_table = 1     # First column of trend table

        # Create sheet
        sheet = self.add_worksheet('trends')
        content = self.estats.getTrends()
        self.insertDisclaimer(sheet)
        self.insertExplaination(sheet, r_expl, c_table, c_cnt = len(content[0])-2)
        self.insertTable(sheet, r_table, c_table, content)
        self.addTrendChart(sheet.name, r_table, c_table)


    def addTrendChart(self, sheet_name, r_table, c_table):
        """ Create trend chart """
        print("Creating trend chart")

        # Generate State chart
        sheet = self.add_chartsheet('trend chart')
        content = self.estats.getTrends()

        r_headers = r_table
        r_data = r_headers+1
        c_first = c_table+1
        c_last = c_table+len(content[0])-1

        chart = self.add_chart({'type': 'line'})
        for emote_idx in range(len(content)-1):
            chart.add_series({
                'categories': [sheet_name, r_headers, c_first, r_headers, c_last], # Date buckets
                'values': [sheet_name, r_data+emote_idx, c_first, r_data+emote_idx, c_last],
                'name': content[emote_idx+1][0],
                'marker': {'type': 'automatic'},
                })
        chart.set_title({'name': 'Trends [count/hour]'})
        chart.set_legend({'position': 'bottom'})
        sheet.set_chart(chart)
