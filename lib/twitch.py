#!/usr/bin/python

import requests
import json
import sys
import time


class Twitch:
    """ Twitch config and API """
    MAX_RETRIES = 20
    BASE_DELAY = 0.1    # Time between retries in Seconds (increased exponentially between retries)
    MAX_DELAY = 10      # Max delay in Seconds

    def __init__(self, config):
        self.clientID = config['clientID']
        self.channel = config['channel']

        self.api_url = "https://api.twitch.tv/helix/{path}"
        self.legacy_api_url = "https://api.twitch.tv/api/{path}"
        self.legacy_apiv5_url = "https://api.twitch.tv/v5/{path}"


        self.user_id = self.getUserId()

    def getUserId(self):
        """ Obtain user_id based on channel name """
        print("Obtaining user_id for {0}".format(self.channel))

        response = self.get("users?login={0}".format(self.channel)).json()
        assert('id' in response['data'][0])
        # print(json.dumps(response, indent=4)) # DEBUG

        return response['data'][0]['id']


    def getVideos(self, cursor = None):
        """ Get list of videos,
            use cursor to start fetching the next result in multi-page response
            returns list of videos and cursor string
        """
        print("Fetch {0} page of videos".format("another" if cursor else "a"))

        params = {'user_id':self.user_id}
        if cursor:
            params['after'] = cursor
        response = self.get("videos", params).json()
        # print(json.dumps(response, indent=4)) # DEBUG

        # note that "data": [] indicates no more videos in current page (still returns valid cursor)
        return response["data"],response["pagination"]["cursor"]


    def getChatLogs(self, video_id, cursor = None):
        """ Get chat log for specific video """
        params = {'cursor': cursor} if cursor else {}
        response = self.get("videos/{0}/comments".format(video_id), params,
                                                     legacy_api_url = self.legacy_apiv5_url).json()
        # print(json.dumps(response, indent=4)) # DEBUG

        return response["comments"], response.get("_next",None)


    def getEmoteCodes(self):
        """ Get list of emote codes based on channel id """
        print("Get list of emote codes based on channel id")
        print("WARNING: using legacy API to fetch emotes codes "
              "-> remedy this once new API supports this")

        response = self.get("channels/cohhcarnage/product",
                             legacy_api_url = self.legacy_api_url)

        emotes = []
        try:
            for plan in json.loads(response._content)['plans']:
                emotes.extend([emote['regex'] for emote in plan['emoticons']])
        except:
            print("\nERROR: failed to extract emote codes from data returned by Twitch API")
            sys.exit(1)
        # print(emotes) # DEBUG

        return emotes


    def get(self, path: str, params: dict = {}, headers: dict = {}, legacy_api_url = None):
        """ Request to twitch API """
        headers['Client-ID'] = self.clientID

        url = legacy_api_url.format(path=path) if legacy_api_url else self.api_url.format(path=path)

        # Allow for API problems (retry with increasing delay)
        retry = 0
        delay = self.BASE_DELAY
        while (True):
            response = requests.get(url, params=params, headers=headers)

            # Unauthorized access
            if response.status_code == 401:
                print('\nERROR: Twitch API returned status code {}. Please check your client ID.'
                      ''.format(response.status_code))
                print('\nUrl\t{}\nParams\t{}\nHeaders\t{}\n'.format(response.url, params, headers))
                sys.exit(1)

            # Other errors
            if response.status_code != requests.codes.ok:
                if retry == self.MAX_RETRIES:
                    print('\nERROR: Twitch API returned status code {}. Max. number of retries '
                          'exceeded.'.format(response.status_code))
                    sys.exit(1)

                if retry == 0:
                    print('\nUrl\t{}\nParams\t{}\nHeaders\t{}\n'
                          ''.format(response.url, params, headers))
                print('WARNING: Twitch API returned status code {0}. Retry in {1}s'
                      ''.format(response.status_code, delay))
                time.sleep(delay);
                retry += 1
                delay = 2*delay if (2*delay < self.MAX_DELAY) else self.MAX_DELAY
                continue
            break

        # # DEBUG
        # import pprint
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(response.__dict__)
        # # END_DEBUG

        return response