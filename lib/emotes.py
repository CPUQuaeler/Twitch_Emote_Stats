#!/usr/bin/python

import sys
import io
import gzip
import os
import math
import datetime
import dateutil.relativedelta

from lib.util import dateToStr


class Emote:
    """ Stats corresponding to one specific emote """

    def __init__(self, parent, config):
        # Buckets corresponding to one or more dates (used to show trends)
        self.buckets = [ 0 for i in range(parent.bucket_count) ]

        self.parent = parent


    def getStats(self):
        """ Return stats corresponding to this emote """
        # Sums over x days from end date
        sum_last_24h = self.buckets[-1]
        sum_last_7d = sum(self.buckets if (self.parent.bucket_count <= 7) else self.buckets[-7:])
        sum_last_1m = sum(self.buckets if (self.parent.bucket_count <= self.parent.days_1m)
                          else self.buckets[-self.parent.days_1m:])
        sum_range = sum(self.buckets)

        # Dates emote was first and last used
        first_used = 'n/a'
        last_used = 'n/a'
        for idx,b in enumerate(self.buckets):
            if b > 0:
                first_used = dateToStr(self.parent.date_from + datetime.timedelta(days=idx))
                break

        for idx,b in enumerate(self.buckets[::-1]):
            if b > 0:
                last_used = dateToStr(self.parent.date_from + datetime.timedelta(days=idx))
                break

        # Note: order should match result of getHeaders()
        return [first_used, last_used, sum_last_24h, sum_last_7d, sum_last_1m, sum_range]


    def getHeaders(self):
        """ Return heads matching stats returned by getStats() """
        return ["First used", "Last used", "Last 24h", "Last week", "Last month", "Within range"]



class EmoteStats:
    """ List of Emotes and corresponding stats """

    def __init__(self, config):
        self.channel = config['channel']
        self.date_from = config['date_from']

        codes = config['keywords'] + config['twitch'].getEmoteCodes()
        self.codes = [code.encode('utf-8') for code in codes]

        # Calculate number of buckets corresponding to one or more dates (used to show trends)
        dates_in_range = (config['date_to'] - config['date_from']).days + 1
        self.bucket_scale = math.ceil(dates_in_range / config['max_date_bucket'])
        self.bucket_count = math.ceil(dates_in_range / self.bucket_scale)

        # Calculate days between end date and same day 1 month prior
        self.days_1m = (config['date_to'] -
                        (config['date_to'] - dateutil.relativedelta.relativedelta(months=1))).days

        self.emote_stats = {code.decode('ascii'): Emote(self, config) for code in self.codes}

        # Track # of video hours per bucket
        self.video_hours = [ 0.0 for i in range(self.bucket_count) ]


    def addVideo(self, video):
        """ Add video to statistic """
        print("Evaluating chat log for video id {0} ({1})".format(video.id, video.date_str))

        assert(os.path.exists(video.file))

        # Index of corresponding date bucket
        bucket_idx = int((video.date.date() -  self.date_from).days / self.bucket_scale)

        with gzip.open(video.file, 'rb') as gz:
            f = io.BufferedReader(gz)
            for line in f:
                # print(line[:-1]) # DEBUG
                for code in self.codes:
                    # Check for code followed by whitespace or endline
                    if any(c in line for c in [code+b' ', code+b'\n']):
                        # Update emote count of corresponding day bucket
                        self.emote_stats[code.decode('ascii')].buckets[bucket_idx] += 1

        # Track video duration in hours
        self.video_hours[bucket_idx] += video.duration / 3600


    def getSums(self):
        """ Return table on stats of all emotes (including headers) """

        assert(len(self.emote_stats) > 0)

        # Get headers from any emote object
        headers = ['Name'] + (next(iter(self.emote_stats.values()))).getHeaders()
        # Get stats for each emote
        rows = [ [name] + value.getStats() for name,value in self.emote_stats.items() ]
        # Sort emotes by descending overall count
        rows.sort(key=lambda x: x[headers.index('Within range')-1], reverse=True)

        return([headers] + rows)


    def getTrends(self):
        """ Return table on usage trends over time (including headers) """
        assert(self.bucket_count > 0)

        # Header = bucket names (average date within bucket)
        cur_header = self.date_from + datetime.timedelta(days=int(self.bucket_scale / 2))
        headers = [ "Name", dateToStr(cur_header) ]
        for i in range(self.bucket_count-1):
            cur_header += datetime.timedelta(days=self.bucket_scale)
            headers.append(dateToStr(cur_header))

        # Get trend for each emote
        rows = [ [name] + [round(cnt/hours,2) for cnt,hours in zip(value.buckets, self.video_hours)]
            for name,value in self.emote_stats.items() ]

        # Sort by name
        rows.sort(key=lambda x: x[0])

        return([headers] + rows)



