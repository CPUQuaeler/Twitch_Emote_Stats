#!/usr/bin/python

import datetime
import sys


def dateToStr(date):
    """ Convert date to string """
    return date.strftime("%Y.%m.%d")


def strToDate(text):
    """ Convert date to string """
    return datetime.datetime.strptime(text, '%Y.%m.%d').date()


def userPromp(text):
    """ Yes/No user prompt """
    while (True):
        choice = input(text).lower()
        if choice in {'yes','y', 'ye', ''}:
            return True
        elif choice in {'no','n'}:
            return False
        text = "Please respond with 'yes' or 'no': "


def draw_progress(cur, max, text):
    """ Draw progress bar """
    sys.stdout.write('[{}] {}%\r'.format(text, '%.2f' % min(cur * 10 / max * 10, 100.00)))
    sys.stdout.flush()


def updDict(dict_in, dict_diff):
    """ Create new dictionary including update of dict_in by dict_diff """
    result = dict(dict_in)
    result.update(dict_diff)
    return result