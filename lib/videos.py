#!/usr/bin/python

import datetime
import sys
import os
import gzip
import re

from lib.util import dateToStr, draw_progress



class Video:
    """ Data corresponding to a single video """

    def __init__(self, parent, video_info, date, config):
        # Regex used to convert string to time for video duration
        self.regex_duration = re.compile(
                r'((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')

        self.id = video_info['id']
        self.url = video_info['url']
        self.date = date # Publication date
        self.date_str = dateToStr(date)
        self.duration = self.durationToSeconds(video_info['duration'])
        self.file = os.path.join(config['log_dir'],
                             "{0}_{1}_{2}.txt.gz".format(config['channel'], self.date_str, video_info['id']))
        self.config = config
        self.parent = parent

        self.checkChatLog()


    def durationToSeconds(self, time_str):
        """ Convert duration string to seconds """
        parts = self.regex_duration.match(time_str)
        assert(parts)

        parts = parts.groupdict()
        time_params = {}
        for (name, param) in parts.items():
            if param:
                time_params[name] = int(param)
        return datetime.timedelta(**time_params).total_seconds()


    def evalLogs(self, file, logs):
        """ Extract useful information from logs returned by twitch """
        if len(logs):
            draw_progress(logs[-1]['content_offset_seconds'], self.duration, "Downloading")

        # writelines() is faster than multiple write()
        # use utf-8 encoding to write in binary mode (breaks newline when reading file normally,
        #                                             but ok when reading in binary mode)
        file.writelines([u'{0}\n'.format(entry['message']['body']).encode('utf-8')
                        for entry in logs])


    def checkChatLog(self):
        """ Check if chat log already exists, download if not """
        if os.path.exists(self.file):
            print("Chat log for video id {0} ({1}) located".format(self.id, self.date_str))
            return

        print("Fetching chat log for video id {0} ({1})".format(self.id, self.date_str))

        if self.parent.warn_once: # Show only once
            print("\tTakes a while as the twitch API only returns a few lines per request")
            print("\tWARNING: using legacy API (v5) to fetch chat log "
                  "-> remedy this once new API supports this") # legacy_apiv5_url in Twitch class
            self.parent.warn_once = False

        # Write output to gzip file
        with gzip.open(self.file, 'wb') as f:
            # Fetch first set of chat logs
            logs,cursor = self.config['twitch'].getChatLogs(self.id)
            self.evalLogs(f, logs)

            # DEBUG
            # print("DEBUG: early exit checkChatLog")
            # return
            # DEBUG_END

            # Stop at empty set
            while (len(logs) and cursor):
                # Fetch next set
                logs,cursor = self.config['twitch'].getChatLogs(self.id, cursor)
                self.evalLogs(f, logs)


class Videos(list):
    """ List of videos """

    def __init__(self, config, *args):
        list.__init__(self, *args)
        self.date_from = config['date_from']
        self.date_to = config['date_to']
        self.config = config
        self.warn_once = True


    def extendTwitchFormat(self, twitch_videos):
        """ Extend list based on video list returned by twitch API
            Ignore videos outside of  date range
            Return False if date prior to date_from found
        """
        for video in twitch_videos:
            # Note conversion from twitch specific format here
            date = datetime.datetime.strptime(video['published_at'][:10], '%Y-%m-%d')

            # Ignore dates after end date
            if date.date() > self.date_to:
                continue
            # Stop at date prior to start date
            if date.date() < self.date_from:
                return False
            # Add video to list
            self.append(Video(self, video, date, self.config))
        return True


